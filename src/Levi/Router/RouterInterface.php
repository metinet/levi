<?php declare(strict_types=1);

namespace Levi\Router;

/**
 * Interface RouterInterface
 * @package Levi\Router
 */
interface RouterInterface
{
    /**
     * @param int $id
     * @param string $slug
     * @return null|string
     */
    public static function buildUrl(int $id, string $slug): ?string;

    /**
     * @param int $id
     * @param string $slug
     * @return null|string
     */
    public static function buildCategoryUrl(int $id, string $slug): ?string;
}
