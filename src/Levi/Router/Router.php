<?php declare(strict_types=1);

namespace Levi\Router;

use Monolog\Logger;

/**
 * Class Router
 * @package Levi\Router
 */
abstract class Router implements RouterInterface
{
    /** @var array */
    private $routes;
    /** @var Logger */
    private $logger;

    /**
     * Router constructor.
     * @param string $routesFilename
     * @param Logger $logger
     */
    public function __construct(string $routesFilename, Logger $logger)
    {
        $this->routes = require $routesFilename;
        $this->logger = $logger;
    }

    /**
     * @param string $name
     * @return string
     * @throws \InvalidArgumentException
     */
    public function get(string $name): string
    {
        if (empty($this->routes[$name])) {

            $message = \sprintf('Route with name %s not found.', $name);
            $this->logger->error($message);
            throw new \InvalidArgumentException($message);
        }
        if (empty($this->routes[$name]['link'])) {
            $message = \sprintf('Route link for name %s is missing.', $name);
            $this->logger->error($message);
            throw new \InvalidArgumentException($message);
        }

        return (string)$this->routes[$name]['link'];
    }

    /**
     * @return string
     */
    public static function getBasePath(): string
    {
        return '/';
    }

    /**
     * @param int $id
     * @param string $slug
     * @return null|string
     */
    public static function buildUrl(int $id, string $slug): ?string
    {
        return self::getBasePath() . '/' . $slug . '-' . $id;
    }
}
