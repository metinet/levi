<?php declare(strict_types=1);

namespace Levi\Router;

use Levi\Menu\Menu;
use Levi\Menu\MenuItem;
use Levi\Translator;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouterMenu
 * @package Levi\Router
 */
class RouterMenu
{
    /** @var RouteCollection */
    private $routeCollection;
    /** @var Translator */
    private $translator;
    /** @var string */
    private $parent;

    /**
     * RouterMenu constructor.
     * @param RouteCollection $routeCollection
     * @param Translator $translator
     */
    public function __construct(RouteCollection $routeCollection, Translator $translator)
    {
        $this->translator = $translator;
        $this->routeCollection = $routeCollection;
    }

    /**
     * @param string $parent
     * @return RouterMenu
     */
    public function fromParent(string $parent): self
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Menu
     */
    public function build(): Menu
    {
        return $this->buildMenu($this->buildHierarchy($this->parent));
    }

    /**
     * @param string $parent
     * @return array
     * @throws \InvalidArgumentException
     */
    private function buildHierarchy(string $parent): array
    {
        $items = [];
        foreach ($this->routeCollection as $key => $item) {

            $chunks = \explode('_', $key);
            if ($parent !== $chunks[0]) {
                continue;
            }

            if (\count($chunks) === 1) {
                $items[$chunks[0]] = [
                    'label' => $this->translator->get($key),
                    'path' => $item->getPath(),
                    'children' => []
                ];
            }

            if (\count($chunks) === 2) {
                $items[$chunks[0]]['children'][$chunks[1]] = [
                    'label' => $this->translator->get($key),
                    'path' => $item->getPath(),
                    'children' => []
                ];
            }
            if (\count($chunks) === 3) {
                $items[$chunks[0]]['children'][$chunks[1]]['children'][$chunks[2]] = [
                    'label' => $this->translator->get($key),
                    'path' => $item->getPath(),
                    'children' => []
                ];
            }
        }

        if (empty($items[$parent])) {
            throw new \InvalidArgumentException(\sprintf('There is no parent with name %s', $parent));
        }
        return $items[$parent];
    }

    /**
     * @param array $items
     * @return Menu
     */
    private function buildMenu(array $items): Menu
    {
        $menu = Menu::factory();
        /** @var array $items */
        foreach ($items['children'] as $key => $item) {
            $first = $menu->add(MenuItem::build($item['label'], $item['path']));
            if (!empty($item['children'])) {
                foreach ($item['children'] as $child) {
                    $second = $first->add(MenuItem::build($child['label'], $child['path']));
                    if (!empty($child['children'])) {
                        foreach ($child['children'] as $childChild) {
                            $second->add(MenuItem::build($childChild['label'], $childChild['path']));
                        }
                    }
                }
            }
        }
        return $menu;
    }
}