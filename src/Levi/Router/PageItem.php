<?php declare(strict_types=1);

namespace Levi\Router;

/**
 * Class PageItem
 * @package Levi\Router
 */
class PageItem
{
    /** @var int */
    private $index;
    /** @var string */
    private $link;
    /** @var string */
    private $name;
    /** @var bool */
    private $isPlaceholder;

    /**
     * PageItem constructor.
     * @param int $index
     * @param string $link
     * @param string $name
     * @param bool|null $isPlaceholder
     */
    public function __construct(int $index, ?string $link, ?string $name = null, bool $isPlaceholder = null)
    {
        $this->index = $index;
        $this->link = $link;
        $this->name = $name;
        $this->isPlaceholder = (bool)$isPlaceholder;
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @return int
     */
    public function getIteration(): int
    {
        return $this->index + 1;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isPlaceholder(): bool
    {
        return $this->isPlaceholder;
    }
}
