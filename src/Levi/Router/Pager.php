<?php declare(strict_types=1);

namespace Levi\Router;

use BadFunctionCallException;
use Levi\Http\Request;
use Levi\Menu\Category;

/**
 * Class Pager
 * @package Levi
 */
class Pager
{
    private const LIMIT = 30;

    /** @var String */
    private $parent;
    /** @var String */
    private $child;
    /** @var integer */
    private $offset;
    /** @var integer */
    private $limit;
    /** @var integer */
    private $page;
    /** @var integer */
    private $count;
    /** @var Category */
    private $category;
    /** @var Category */
    private $subcategory;
    /** @var PageItem[] */
    private $items;
    /** @var integer */
    private $pageCount = 0;

    /**
     * Pager constructor.
     * @param int|null $page
     * @param int|null $offset
     * @param int|null $limit
     * @param null|string $parent
     * @param null|string $child
     * @param int|null $count
     */
    private function __construct(
        ?int $page,
        ?int $offset,
        ?int $limit,
        ?string $parent,
        ?string $child,
        ?int $count = null
    )
    {
        $this->offset = $offset;
        $this->limit = $limit;
        $this->count = $count;
        $this->page = $page;
        $this->parent = $parent;
        $this->child = $child;
    }

    /**
     * @param Request $request
     * @return Pager
     */
    public static function fromRequest(Request $request): Pager
    {
        $page = (int)$request->get('page');

        $offset = $page * self::LIMIT;
        $limit = self::LIMIT;
        return new self($page, $offset, $limit, $request->get('pcat'), $request->get('ccat'));
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return $this->parent;
    }

    /**
     * @return null|string
     */
    public function getChild(): ?string
    {
        return $this->child;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return Category
     */
    public function getSubcategory(): Category
    {
        return $this->subcategory;
    }

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        if (empty($this->category->getName())) {
            return '';
        }

        if (!empty($this->subcategory->getName())) {
            return \trim($this->category->getName() . ' &gt; ' . $this->subcategory->getName());
        }

        return \trim($this->category->getName());
    }

    /**
     * @return String
     */
    public function getCategoryDescription(): string
    {
        if (empty($this->subcategory->getDescription())) {
            return $this->category->getDescription() ?? $this->parent;
        }

        return $this->subcategory->getDescription();
    }

    /**
     * @return String
     */
    public function getCategoryKeywords(): string
    {
        if (empty($this->subcategory->getKeywords())) {
            return $this->category->getKeywords() ?? $this->parent;
        }

        return $this->subcategory->getKeywords();
    }

    /**
     * @param Category|null $category
     * @param Category|null $subcategory
     * @return Pager
     */
    public function withCategory(Category $category = null, Category $subcategory = null): Pager
    {
        $this->category = $category;
        $this->subcategory = $subcategory;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasParent(): bool
    {
        return null !== $this->parent;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return \json_encode(\get_object_vars($this));
    }

    /**
     * @return Router[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getPageCount(): int
    {
        return $this->pageCount;
    }

    /**
     * @param string $routerClass
     * @return Pager
     * @throws \BadFunctionCallException
     */
    public function withLinkItems(string $routerClass): self
    {
        if ((int)$this->count === 0) {
            throw new BadFunctionCallException('Cannot build pager list without result count.');
        }
        $this->items = $this->buildItems($routerClass);

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return Pager
     */
    public function withResultCount(int $count): self
    {
        $this->count = $count;
        $this->pageCount = (int)\ceil($count / $this->getLimit());

        return $this;
    }

    /**
     * @param string $routerClass
     * @return array
     */
    private function buildItems(string $routerClass): array
    {
        /** @var RouterInterface $routerClass */
        $link = $routerClass::buildCategoryUrl(...[$this->getParent(), $this->getChild()]);
        $items = [
            new PageItem(0, \sprintf('%s', $link))
        ];
        for ($i = 1; $i < $this->pageCount - 1; $i++) {
            $items[] = new PageItem($i, \sprintf('%s?%s', $link, \http_build_query(['page' => $i + 1])));
        }

        return $items;
    }
}
