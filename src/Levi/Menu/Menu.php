<?php declare(strict_types=1);

namespace Levi\Menu;

/**
 * Class Menu
 * @package App\Menu
 */
class Menu implements \Iterator
{
    /** @var MenuItem[] */
    private $items;
    /** @var int */
    private $position = 0;
    /** @var int */
    private $count;
    /** @var array */
    private $routeCollection;

    /**
     * Menu constructor.
     * @param MenuItem[] $items
     */
    private function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return Menu
     */
    public static function factory(): self
    {
        return new self([]);
    }

    /**
     * @param MenuItem $item
     * @return MenuItem
     */
    public function add(MenuItem $item): MenuItem
    {
        $this->items[] = $item;
        $this->count = \count($this->items);
        return $item;
    }

    /**
     * @return MenuItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $position
     * @return MenuItem
     */
    public function get(int $position): MenuItem
    {
        return $this->items[$position];
    }

    /**
     * @return MenuItem
     */
    public function current(): MenuItem
    {
        return $this->items[$this->position];
    }

    /**
     *
     */
    public function next(): void
    {
        $this->position++;
    }

    /**
     * @return int
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->items[$this->position]);
    }

    /**
     *
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * @param array $collection
     * @return Menu
     */
    public function fromRouteCollection(array $collection): Menu
    {
        $this->routeCollection = $collection;
        return $this;
    }

    /**
     * @param string $string
     * @return Menu
     */
    public function fromPart(string $string): Menu
    {
        $this->items = $this->routeCollection[$string];
        return $this;
    }
}
