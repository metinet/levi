<?php declare(strict_types=1);

namespace Levi\Menu;

use Levi\HtmlTag;

/**
 * Class MenuItem
 * @package Levi\Menu
 */
class MenuItem
{
    /** @var string */
    private $linktext;
    /** @var string */
    private $url;
    /** @var string */
    private $title;
    /** @var string */
    private $className;
    /** @var int */
    private $counter;
    /** @var array */
    private $attr;
    /** @var self[] */
    private $children;

    /**
     * MenuItem constructor.
     * @param string $linktext
     * @param string $url
     * @param string $title
     * @param string $className
     * @param int $counter
     * @param array $attr
     * @throws \InvalidArgumentException
     */
    private function __construct(
        string $linktext,
        string $url = null,
        string $title = null,
        string $className = null,
        int $counter = null,
        array $attr = null
    )
    {
        if (empty($linktext)) {
            throw new \InvalidArgumentException('MenuItem at least needs link text.');
        }

        $this->url = $url;
        $this->linktext = $linktext;
        $this->title = $title;
        $this->className = $className;
        $this->counter = $counter;
        $this->attr = $attr;
    }

    /**
     * @param string $linktext
     * @param string $url
     * @param string $title
     * @param string|null $classes
     * @param int|null $counter
     * @param array|null $attr
     * @return MenuItem
     */
    public static function build(
        string $linktext,
        string $url = null,
        string $title = null,
        string $classes = null,
        ?int $counter = null,
        ?array $attr = null
    ): MenuItem
    {
        return new self($linktext, $url, $title, $classes, $counter, $attr);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getLinktext(): string
    {
        return $this->linktext;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getClassName(): ? string
    {
        return $this->className;
    }

    /**
     * @return int
     */
    public function getCounter(): ? int
    {
        return $this->counter;
    }

    /**
     * @return array
     */
    public function getAttr(): array
    {
        return $this->attr;
    }

    /**
     * @param array|null $attributes
     * @return HtmlTag
     */
    public function buildHtml(array $attributes = null): HtmlTag
    {
        if ($this->counter !== null) {
            $this->linktext .= ' ' . \sprintf(' <span>(%s)</span>', $this->counter);
        }

        $title = null;
        if ($this->title !== null) {
            $title = \trim($this->title);
        }
        $tag = HtmlTag::create('a', [
            'href' => $this->url,
            'title' => $title,
            'class' => $this->className
        ])
            ->withText($this->linktext)
            ->withAttributes($attributes);

        return $tag;
    }

    /**
     * @param MenuItem[] $children
     * @return MenuItem
     */
    public function setChildren(array $children): MenuItem
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @param MenuItem $item
     * @return MenuItem
     */
    public function add(MenuItem $item): MenuItem
    {
        if ($this->children === null) {
            $this->children = [];
        }

        $this->children[] = $item;

        return $this;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @return bool
     */
    public function hasChildren(): bool
    {
        return $this->children !== null;
    }
}
