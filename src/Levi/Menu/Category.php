<?php declare(strict_types=1);

namespace Levi\Menu;

/**
 * Class Category
 * @package Levi\Menu
 */
class Category
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $description;
    /** @var string */
    private $keywords;
    /** @var string */
    private $url;
    /** @var null|string */
    private $parentName;
    /** @var null|string */
    private $parentUrlName;
    /** @var null|string */
    private $parentUrl;
    /**
     * @var null|string
     */
    private $urlName;

    /**
     * Category constructor.
     * @param int|null $id
     * @param string|null $name
     * @param string|null $description
     * @param string|null $keywords
     * @param string|null $urlname
     * @param string|null $url
     * @param string|null $parentName
     * @param string|null $parentUrlName
     * @param string|null $parentUrl
     */
    private function __construct(
        int $id = null,
        string $name = null,
        string $description = null,
        string $keywords = null,
        string $urlname = null,
        string $url = null,
        string $parentName = null,
        string $parentUrlName = null,
        string $parentUrl = null
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->keywords = $keywords;
        $this->url = $url;
        $this->parentName = $parentName;
        $this->parentUrlName = $parentUrlName;
        $this->parentUrl = $parentUrl;
        $this->urlName = $urlname;
    }

    /**
     * @param $item
     * @return Category
     */
    public static function fromItem($item): Category
    {
        $url = null;
        $parentUrl = null;
        if (!empty($item->parent_urlname)) {
            $parentUrl = \sprintf('/c/%s', $item->parent_urlname);
        }
        if ($item->urlname !== null && !empty($item->parent_urlname)) {
            $url = \sprintf('/c/%s/%s', $item->parent_urlname, $item->urlname);
        }

        if (empty($item->parent_urlname)) {
            $url = \sprintf('/c/%s', $item->urlname);
        }

        return new self(
            (integer)$item->id,
            $item->name ?? null,
            $item->description ?? null,
            $item->keywords ?? null,
            $item->urlname,
            $url,
            $item->parent_name ?? null,
            $item->parent_urlname ?? null,
            $parentUrl ?? null
        );
    }

    /**
     * @return Category
     */
    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->name === null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name ?? '';
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords ?? '';
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url ?? '';
    }

    /**
     * @return string
     */
    public function getDirectoryUrl(): string
    {
        return \str_replace('/c/', '/cb/', $this->url ?? '');
    }

    /**
     * @return null|string
     */
    public function getParentName(): ?string
    {
        return $this->parentName;
    }

    /**
     * @return null|string
     */
    public function getParentUrlName(): ?string
    {
        return $this->parentUrlName;
    }

    /**
     * @return null|string
     */
    public function getParentUrl(): ?string
    {
        return $this->parentUrl;
    }

    /**
     * @return null|string
     */
    public function getUrlName(): ?string
    {
        return $this->urlName;
    }
}
