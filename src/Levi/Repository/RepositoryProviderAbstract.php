<?php declare(strict_types=1);

namespace Levi\Repository;

use Levi\Config\DatabaseConfig;
use Monolog\Logger;

/**
 * Class RepositoryProviderAbstract
 * @package Levi\Repository
 */
class RepositoryProviderAbstract implements RepositoryProviderInterface
{
    /** @var DatabaseConfig */
    private $config;
    /** @var Logger */
    private $logger;
    /** @var PDORepository[] */
    protected static $repositories;

    /**
     * RepositoryProviderAbstract constructor.
     * @param DatabaseConfig $config
     * @param Logger $logger
     */
    public function __construct(DatabaseConfig $config, Logger $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param string $className
     * @return PDORepository
     */
    public function factory(string $className): PDORepository
    {
        if (empty(self::$repositories[$className])) {
            self::$repositories[$className] = new $className($this->getConfig(), $this->getLogger());
        }
        return self::$repositories[$className];
    }

    /**
     * @return DatabaseConfig
     */
    public function getConfig(): DatabaseConfig
    {
        return $this->config;
    }

    /**
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return $this->logger;
    }
}
