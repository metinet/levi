<?php declare(strict_types=1);

namespace Levi\Repository;

/**
 * Interface RepositoryProviderInterface
 * @package Levi\Repository
 */
interface RepositoryProviderInterface
{
    /**
     * @param string $className
     * @return object
     */
    public function factory(string $className);
}