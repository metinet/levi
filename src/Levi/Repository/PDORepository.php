<?php declare(strict_types=1);

namespace Levi\Repository;

use Levi\Router\Pager;
use Levi\Config\DatabaseConfig;
use Monolog\Logger;
use PDO;
use PDOStatement;

/**
 * Class PDORepository
 * @package Levi\Repository
 */
abstract class PDORepository implements PDORepositoryInterface
{
    /** @var PDO */
    private static $pdo;
    /** @var DatabaseConfig */
    private $config;
    /** @var Logger */
    private $logger;
    /** @var bool */
    private $enableDebug;
    /** @var float */
    private static $startTime;

    /**
     * PDORepository constructor.
     * @param DatabaseConfig $config
     * @param Logger $logger
     */
    public function __construct(DatabaseConfig $config, Logger $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @return PDO
     */
    private function getConnection(): PDO
    {
        if (self::$pdo === null) {
            try {
                self::$pdo = new PDO(
                    $this->config->getDsn(),
                    $this->config->getUser(),
                    $this->config->getPassword(),
                    [
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $this->config->getCharset(),
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    ]
                );
            } catch (\Exception $e) {
                echo $e->getMessage();
                die;
            }
        }

        return self::$pdo;
    }

    /**
     * @param string $sql
     * @param array|null $args
     * @return Result
     */
    protected function fetchAll(string $sql, array $args = null): Result
    {
        $statement = $this->execute($sql, $args);

        return new Result($statement);
    }

    /**
     * @param string $sql
     * @param array|null $args
     * @return \stdClass
     */
    protected function fetchOne(string $sql, array $args = null): \stdClass
    {
        $object = $this->execute($sql, $args)->fetchObject();

        if (empty($object)) {
            return new \stdClass();
        }
        return $object;
    }

    /**
     * @param string $sql
     * @param array|null $args
     * @return PDOStatement
     */
    private function execute(string $sql, array $args = null): \PDOStatement
    {
        if ($this->enableDebug) {
            self::$startTime = \microtime(true);
        }
        $connection = $this->getConnection();
        $stmt = $connection->prepare(\trim($sql));
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        try {
            $stmt->execute($args);
        } catch (\PDOException $e) {
            $this->logger->error(static::class . ': ' . \trim($e->getMessage()));
            if (!$this->enableDebug && !$this->config->isDebug()) {
                echo '<div style="background-color: #ff1300; color: #f0f0f0; padding: 3px;">' . static::class . '<br>';
                echo '' . $e->getMessage() . '<br>';
                echo '<pre style="font-size: 12px;">' . \print_r($stmt, true) . ' </pre></div>';
            }
        }

        if ($this->enableDebug || $this->config->isDebug()) {
            $this->displayDebug($stmt, $args);
        }

        return $stmt;
    }

    /**
     *
     */
    public function debug()
    {
        $this->enableDebug = true;
    }

    /**
     * @param PDOStatement $stmt
     * @param array|null $args
     */
    private function displayDebug(PDOStatement $stmt, array $args = null)
    {
        echo '<pre><h1>' . static::class . '</h1></pre>';
        if ((int)$stmt->errorCode() > 0) {
            $msg = '<div style="background-color: #ff1300; padding: 5px 7px; border-radius: 3px;">';
            $msg .= '<strong style="font-size: 12px; color: #f0f0f0">' . $stmt->errorInfo()[2] . ' </strong>';
            echo $msg;
        } else {
            echo '<div>';
        }
        echo '<pre style="font-size: 11px; background-color: #c2e7bb; padding: 5px 7px; border-radius: 3px;">';
        echo \SqlFormatter::format($stmt->queryString, false);
        echo '</pre>';
        if (!empty($args)) {
            echo '<pre style="font-size: 11px; background-color: #f0bdd2; padding: 5px 7px; border-radius: 3px;">';
            echo '<br><strong>Args </strong>' . \print_r($args, true) . ' ';
            echo '</pre>';
        }
        echo '<table cellpadding="0" cellspacing="0" style="width: 100%;padding: 0; margin: 0">';

        echo '<tbody>';

        foreach ($stmt->fetchAll() as $record) {
            echo '<tr style="display: grid;">';
            /** @noinspection ForgottenDebugOutputInspection */
            echo '<td  style="padding: 0">
                <pre style="font-size: 11px; background-color: #7b9ec0; padding: 5px 7px; border-radius: 3px;">' . \print_r($record, true) . '</pre>
                </td>';
            echo '</tr>';
        }
        echo '</tbody>';

        echo '<thead>';
        $rows = $stmt->rowCount() ?? 0;
        $cols = $stmt->columnCount() ?? 0;
        $time = \sprintf('%0.4fs', \microtime(true) - self::$startTime);
        echo '<tr>
                <td>
                <pre style="font-size: 11px; background-color: #c07b9e; padding: 5px 7px; border-radius: 3px;">Rows: ' . $rows . '; Cols: ' . $cols . '; Time: ' . $time . '</pre></td>
               </tr>';
        echo '</thead>';

        echo '</table></div>';
        $this->enableDebug = false;
    }

    /**
     * @param Pager $pager
     * @return string
     */
    protected function appendPagerQuery(Pager $pager): string
    {
        return \sprintf(' LIMIT %s, %s ', $pager->getOffset(), $pager->getLimit());
    }

    /**
     * @param $query
     * @param $args
     * @param Pager $pager
     * @return Result
     */
    protected function fetchPaged(string $query, $args, Pager $pager): Result
    {
        $stmt = $this->getConnection()->prepare(\trim($query));
        $stmt->execute($args);

        $query .= $this->appendPagerQuery($pager);
        return $this->fetchAll($query, $args)
            ->withCountAll($stmt->rowCount());
    }
}
