<?php declare(strict_types=1);

namespace Levi\Repository;

/**
 * Class Result
 * @package Levi\Repository
 */
class Result
{
    /** @var \PDOStatement */
    private $stmt;
    /** @var int */
    private $countAll;

    /**
     * Result constructor.
     * @param $stmt
     */
    public function __construct($stmt)
    {
        $this->stmt = $stmt;
    }

    /**
     * @return \Generator
     */
    public function fetchAll(): ?\Generator
    {
        foreach ($this->stmt->fetchAll() as $record) {
            yield $record;
        }
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->stmt->rowCount();
    }

    /**
     * @param int $countAll
     * @return Result
     */
    public function withCountAll(int $countAll): self
    {
        $this->countAll = $countAll;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountAll(): int
    {
        return $this->countAll;
    }
}