<?php declare(strict_types=1);

namespace Levi\Model;
/**
 * Interface Entity
 * @package Levi\Model
 */
interface Entity
{
    /**
     * @return bool
     */
    public function isEmpty(): bool;
}
