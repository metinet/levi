<?php declare(strict_types=1);

namespace Levi\Mapper;

use Levi\Model\Entity;
use Levi\Repository\Result;
use stdClass;

/**
 * Interface Mapper
 * @package Levi\Mapper
 */
interface Mapper
{
    /**
     * @param stdClass $item
     * @return mixed
     */
    public static function map(stdClass $item);

    /**
     * @param Result $result
     * @return mixed
     */
    public static function mapList(Result $result);
}
