<?php declare(strict_types=1);

namespace Levi\Server;

/**
 * Class Environment
 * @package Levi\Server
 */
class Environment
{
    public const ENV_TESTING = 'testing';
    public const ENV_STAGING = 'staging';
    public const ENV_PRODUCTION = 'production';

    /** @var array */
    private $production = ['production', 'prod'];
    /** @var array */
    private $staging = ['staging', 'stage'];
    /** @var array */
    private $testing = ['testing', 'test'];
    /** @var array */
    private $development = ['development', 'dev'];
    /** @var string */
    private $env;

    /**
     * Environment constructor.
     * @param $env
     */
    private function __construct($env)
    {
        $this->env = $env;
    }

    /**
     * @param $env
     * @return Environment
     */
    public static function fromString($env): Environment
    {
        return new Environment($env);
    }

    /**
     * @return Environment
     */
    public static function fromGlobals(): Environment
    {
        return new self(empty($_SERVER['APP_ENV']) ? 'production' : $_SERVER['APP_ENV']);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->env;
    }

    /**
     * @return bool
     */
    public function isNotProduction(): bool
    {
        return !$this->isProduction();
    }

    /**
     * @return bool
     */
    public function isProduction(): bool
    {
        return \in_array($this->env, $this->production, true);
    }

    /**
     * @return bool
     */
    public function isNotDevelopment(): bool
    {
        return !$this->isDevelopment();
    }

    /**
     * @return bool
     */
    public function isDevelopment(): bool
    {
        return \in_array($this->env, $this->development, true);
    }

    /**
     * @return bool
     */
    public function isNotTesting(): bool
    {
        return !$this->isTesting();
    }

    /**
     * @return bool
     */
    public function isTesting(): bool
    {
        return \in_array($this->env, $this->testing, true);
    }

    /**
     * @return bool
     */
    public function isNotStaging(): bool
    {
        return !$this->isStaging();
    }

    /**
     * @return bool
     */
    public function isStaging(): bool
    {
        return \in_array($this->env, $this->staging, true);
    }
}
