<?php declare(strict_types=1);

namespace Levi\Http;

/**
 * Class Request
 * @package Levi\Http
 */
class Request extends \Symfony\Component\HttpFoundation\Request
{
    public const METHOD_DELETE = 'DELETE';
    public const METHOD_PUT = 'PUT';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_POST = 'POST';
    public const METHOD_OPTIONS = 'OPTIONS';
    public const METHOD_HEAD = 'HEAD';
    public const METHOD_GET = 'GET';

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->get('id');
    }
}
