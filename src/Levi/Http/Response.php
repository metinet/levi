<?php declare(strict_types=1);

namespace Levi\Http;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class Response
 * @package Phinky\Http
 */
class Response extends SymfonyResponse
{

    /**
     * @param null $data
     * @return SymfonyResponse
     */
    public static function ok($data = null): SymfonyResponse
    {
        return self::create($data, self::HTTP_OK);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function noContent($data = null): Response
    {
        $statusCode = self::HTTP_NO_CONTENT;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function badRequest($data = null): Response
    {
        $statusCode = self::HTTP_BAD_REQUEST;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function serviceUnavailable($data = null): Response
    {
        $statusCode = self::HTTP_SERVICE_UNAVAILABLE;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function internalServerError($data = null): Response
    {
        $statusCode = self::HTTP_INTERNAL_SERVER_ERROR;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function unauthorized($data = null): Response
    {
        $statusCode = self::HTTP_UNAUTHORIZED;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function methodNotAllowed($data = null): Response
    {
        $statusCode = self::HTTP_METHOD_NOT_ALLOWED;

        return new self(self::getResponse($data), $statusCode);
    }


    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function notFound($data = null): Response
    {
        $statusCode = self::HTTP_NOT_FOUND;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function unprocessableEntity($data = null): Response
    {
        $statusCode = self::HTTP_UNPROCESSABLE_ENTITY;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function notModified($data = null): Response
    {
        $statusCode = self::HTTP_NOT_MODIFIED;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function created($data = null): Response
    {
        $statusCode = self::HTTP_CREATED;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param null $data
     * @return Response
     * @throws \InvalidArgumentException
     */
    public static function conflict($data = null): Response
    {
        $statusCode = self::HTTP_CONFLICT;

        return new self(self::getResponse($data), $statusCode);
    }

    /**
     * @param $responseData
     * @return string
     */
    private static function getResponse($responseData): string
    {
        if ($responseData !== null && $responseData !== '') {
            return $responseData;
        }

        return '';
    }

    /**
     * @param $appVersion
     */
    public function addVersion($appVersion)
    {
        $this->headers->set('X-Version', $appVersion);
    }
}
