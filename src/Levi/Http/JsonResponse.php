<?php declare(strict_types=1);

namespace Levi\Http;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class JsonResponse
 * @package Levi\Http
 */
class JsonResponse extends Response
{
    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function ok($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_OK;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function noContent($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_NO_CONTENT;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function badRequest($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_BAD_REQUEST;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function serviceUnavailable($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_SERVICE_UNAVAILABLE;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function internalServerError($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_INTERNAL_SERVER_ERROR;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function unauthorized($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_UNAUTHORIZED;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function methodNotAllowed($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_METHOD_NOT_ALLOWED;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function notFound($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_NOT_FOUND;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function notModified($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_NOT_MODIFIED;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function preconditionFailed($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_PRECONDITION_FAILED;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function conflict($data = null): JsonResponse
    {
        $statusCode = StatusCode::HTTP_CONFLICT;

        return self::getResponse($data, $statusCode);
    }

    /**
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function created(): JsonResponse
    {
        $statusCode = StatusCode::HTTP_CREATED;

        return self::getResponse(null, $statusCode);
    }

    /**
     * @param $responseData
     * @param $statusCode
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    private static function getResponse($responseData, $statusCode): JsonResponse
    {
        $data = null;
        if ($responseData !== null && $responseData !== '') {
            $data = \json_encode($responseData);
        }

        $response = new self($data);
        $response->setStatusCode($statusCode);
        return $response;
    }

    /**
     * @param $statusCode
     * @param null $data
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public static function byStatusCode($statusCode, $data = null): JsonResponse
    {
        return self::getResponse($data, $statusCode);
    }
}
