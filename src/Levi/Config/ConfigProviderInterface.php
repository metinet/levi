<?php declare(strict_types=1);

namespace Levi\Config;

/**
 * Interface ConfigProviderInterface
 * @package Levi\Config
 */
interface ConfigProviderInterface
{
    /**
     * ConfigProviderInterface constructor.
     * @param ConfigLoader $configLoader
     */
    public function __construct(ConfigLoader $configLoader);

    /** @return DatabaseConfig */
    public function getDatabaseConfig(): DatabaseConfig;

    /** @return ApplicationConfig */
    public function getApplicationConfig(): ApplicationConfig;
}
