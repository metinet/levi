<?php declare(strict_types=1);

namespace Levi\Config;

/**
 * Class DatabaseConnectionConfig
 * @package Levi\Config
 */
class DatabaseConfig
{
    /** @var string */
    private $driver;
    /** @var string */
    private $host;
    /** @var string */
    private $dbname;
    /** @var string */
    private $user;
    /** @var string */
    private $password;
    /** @var int */
    private $port;
    /** @var boolean */
    private $debug;
    /** @var string */
    private $charset;

    /**
     * DatabaseConfig constructor.
     * @param string $driver
     * @param string $host
     * @param string $dbname
     * @param string $user
     * @param string $password
     * @param int|null $port
     * @param bool|null $debug
     * @param string|null $charset
     */
    public function __construct(
        string $driver,
        string $host,
        string $dbname,
        string $user,
        string $password,
        int $port = null,
        bool $debug = null,
        string $charset = null
    )
    {
        $this->driver = $driver;
        $this->host = $host;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;
        $this->port = 3306;
        if ($port > 0 && $port !== null) {
            $this->port = $port;
        }
        $this->debug = $debug ?? false;
        $this->charset = $charset ?? 'utf8';
    }

    /**
     * @return string
     */
    public function getDriver(): string
    {
        return $this->driver;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getDbname(): string
    {
        return $this->dbname;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @return string
     */
    public function getCharset(): string
    {
        return $this->charset;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'driver' => $this->driver,
            'dbname' => $this->dbname,
            'host' => $this->host,
            'user' => $this->user,
            'password' => $this->password,
            'port' => $this->port,
            'debug' => $this->debug,
            'charset' => $this->charset,
            'dsn' => $this->getDsn()
        ];
    }

    /**
     * @return string
     */
    public function getDsn(): string
    {
        $port = null !== $this->port ? (';port=' . $this->port) : '';
        $dsn = $this->driver . ':host=' . $this->host . $port .
            ';dbname=' . $this->dbname . ';user=' . $this->user . ';password=' . $this->password;

        return $dsn;
    }

    /**
     * @return $this
     */
    public function debug(): DatabaseConfig
    {
        $this->debug = true;

        return $this;
    }
}
