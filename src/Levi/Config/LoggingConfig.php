<?php declare(strict_types=1);

namespace Levi\Config;

/**
 * Class LoggingConfig
 * @package Levi\Config
 */
class LoggingConfig
{
    /** @var string */
    private $logFile;
    /** @var array */
    private $alertMailRecipients;
    /** @var string */
    private $alertMailSender;
    /** @var bool */
    private $debug;
    /** @var string */
    private $dispayErrors;

    /**
     * LoggingConfig constructor.
     * @param $logFile
     * @param $debug
     * @param array|null $alertMailRecipients
     * @param string $alertSender
     * @param bool $dispayErrors
     */
    public function __construct(
        $logFile,
        $debug,
        array $alertMailRecipients = null,
        $alertSender = '',
        $dispayErrors = false
    )
    {
        $this->logFile = $logFile;
        $this->debug = $debug;
        $this->alertMailRecipients = (array)$alertMailRecipients;
        $this->alertMailSender = $alertSender;
        $this->dispayErrors = $dispayErrors;
    }

    /**
     * @return string
     */
    public function getLogFile(): string
    {
        return $this->logFile;
    }

    /**
     * @return bool
     */
    public function getDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @return array
     */
    public function getAlertMailRecipients(): array
    {
        return $this->alertMailRecipients;
    }

    /**
     * @return string
     */
    public function getAlertMailSender(): string
    {
        return $this->alertMailSender;
    }

    /**
     * @return string
     */
    public function getDisplayErrors(): string
    {
        return $this->dispayErrors;
    }
}
