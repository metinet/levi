<?php declare(strict_types=1);

namespace Levi\Config;

/**
 * Class ApplicationConfig
 * @package Levi\Config
 */
class ApplicationConfig
{
    /** @var string */
    private $appPath;
    /** @var string */
    private $httpPath;
    // TODO move to log config
    /** @var string */
    private $logPath;
    /** @var bool */
    private $enableDebugbar;
    /** @var bool */
    private $displayErrors;

    /**
     * ApplicationConfig constructor.
     * @param string $appPath
     * @param string $httpPath
     * @param string $logPath
     * @param bool|null $enableDebugbar
     * @param bool|null $displayErrors
     */
    public function __construct(
        string $appPath,
        string $httpPath,
        string $logPath,
        bool $enableDebugbar = null,
        bool $displayErrors = null)
    {
        $this->logPath = $logPath;
        $this->appPath = $appPath;
        $this->httpPath = $httpPath;
        $this->enableDebugbar = $enableDebugbar;
        $this->displayErrors = $displayErrors ?? false;
    }

    /**
     * @return string
     */
    public function getLogPath(): string
    {
        return __DIR__ . '/../../../' . $this->logPath;
    }

    /**
     * @return string
     */
    public function getAppPath(): string
    {
        return __DIR__ . '/../../../' . $this->appPath;
    }

    /**
     * @return string
     */
    public function getHttpPath(): string
    {
        return __DIR__ . '/../../../' . $this->httpPath;
    }

    /**
     * @return bool
     */
    public function enableDebugbar(): bool
    {
        return $this->enableDebugbar ?? false;
    }

    /**
     * @return bool
     */
    public function shouldDisplayError(): bool
    {
        return $this->displayErrors;
    }
}
