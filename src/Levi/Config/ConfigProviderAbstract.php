<?php declare(strict_types=1);

namespace Levi\Config;

use Exception;
use InvalidArgumentException;
use OutOfBoundsException;

/**
 * Class ConfigProviderAbstract
 * @package Levi\Config
 */
abstract class ConfigProviderAbstract implements ConfigProviderInterface
{
    /** @var array */
    private $configData;
    /** @var ConfigLoader */
    private $configLoader;

    /**
     * ConfigProviderAbstract constructor.
     * @param ConfigLoader $configLoader
     */
    public function __construct(ConfigLoader $configLoader)
    {
        $this->configLoader = $configLoader;
    }

    /**
     * @param array ...$keys
     * @return array
     * @throws \InvalidArgumentException
     * @throws \OutOfBoundsException
     */
    protected function getConfigValue(...$keys): array
    {
        $config = $this->getConfigData();
        foreach ($keys as $key) {
            if (!\array_key_exists($key, $config)) {
                throw new OutOfBoundsException(\sprintf('Unknown config key %s', $key));
            }
            $config = $config[$key];
        }

        return $config;
    }

    /**
     * @param array ...$keys
     * @return string|bool
     * @throws InvalidArgumentException
     */
    protected function getOptionalConfigValue(...$keys)
    {
        $config = $this->getConfigData();
        foreach ($keys as $key) {
            if (!\array_key_exists($key, $config)) {
                return null;
            }
            $config = $config[$key];
        }

        return $config;
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     */
    private function getConfigData(): array
    {
        if ($this->configData === null) {
            try {
                $this->configData = $this->configLoader->load();
            } catch (Exception $e) {
                throw new InvalidArgumentException($e->getMessage(), 0, $e);
            }
        }

        return $this->configData;
    }
}
