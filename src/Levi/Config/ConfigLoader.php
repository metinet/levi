<?php declare(strict_types=1);

namespace Levi\Config;

use InvalidArgumentException;
use Levi\Server\Environment;

/**
 * Class ConfigLoader
 * @package Levi
 */
class ConfigLoader
{
    /** @var string */
    private $environmentName;
    /** @var string */
    private $rootPath;
    /** @var string */
    private $configPath;

    /**
     * ConfigLoader constructor.
     * @param Environment $environment
     * @param $configPath
     * @param $rootPath
     */
    public function __construct(Environment $environment, $configPath, $rootPath)
    {
        $this->environmentName = $environment->toString();
        $this->configPath = \str_replace(
            \DIRECTORY_SEPARATOR . \DIRECTORY_SEPARATOR,
            \DIRECTORY_SEPARATOR,
            $configPath . \DIRECTORY_SEPARATOR
        );
        $this->rootPath = \str_replace(
            \DIRECTORY_SEPARATOR . \DIRECTORY_SEPARATOR,
            \DIRECTORY_SEPARATOR,
            $rootPath . \DIRECTORY_SEPARATOR
        );
    }

    /**
     * @return array
     * @throws InvalidArgumentException
     */
    public function load(): array
    {
        $configFile = $this->getConfigFilePath('config.json');
        $config = \json_decode(\file_get_contents($configFile), true);

        if (!\is_array($config) || \json_last_error() === \JSON_ERROR_STATE_MISMATCH) {
            throw new InvalidArgumentException('Config file ' . basename($configFile) . ' is no valid json'); //@codeCoverageIgnore
        }

        $overwriteFileName = $this->getConfigFilePath('config_' . $this->environmentName . '.json');

        $configOverwrite = [];
        if (is_file($overwriteFileName)) {
            $configOverwrite = \json_decode(\file_get_contents($overwriteFileName), true);
        }

        if (!\is_array($configOverwrite) || \json_last_error() === \JSON_ERROR_STATE_MISMATCH) {
            throw new InvalidArgumentException('Config file ' . \basename($overwriteFileName) . ' is no valid json');
        }

        return \array_replace_recursive($config, $configOverwrite);
    }

    /**
     * @param string $file
     * @return mixed
     */
    public function getConfigFilePath($file = null)
    {
        $path = $this->configPath . ($file ?? '');

        return \str_replace(
            \DIRECTORY_SEPARATOR . \DIRECTORY_SEPARATOR,
            \DIRECTORY_SEPARATOR,
            $path
        );
    }

    /**
     * @return string
     */
    public function getEnvironmentName(): string
    {
        return $this->environmentName;
    }

    /**
     * @return string
     */
    public function getRootPath(): string
    {
        return $this->rootPath;
    }
}
