<?php declare(strict_types=1);

namespace Levi\View;

use Levi\Http\StatusCode;
use Smarty;

/**
 * Class View
 * @package Levi\View
 */
class View
{
    /** @var Smarty */
    private $smarty;
    /** @var int */
    private $statusCode;

    /**
     * View constructor.
     * @param Smarty $smarty
     */
    public function __construct(Smarty $smarty)
    {
        $this->smarty = $smarty;
    }

    /**
     * @param string $template
     * @param int|null $statusCode
     * @return ViewResponse
     */
    public function render(string $template, int $statusCode = null): ViewResponse
    {
        try {
            $html = $this->smarty->fetch($template);
            return ViewResponse::create($html, $statusCode ?? $this->statusCode ?? null);
        } catch (\Exception $e) {
            return ViewResponse::create($e->getMessage())
                ->withStatus(StatusCode::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return Smarty
     */
    public function getSmarty(): Smarty
    {
        return $this->smarty;
    }

    /**
     * @param string $key
     * @param $value
     */
    public function assign(string $key, $value): void
    {
        $this->smarty->assign($key, $value);
    }

    /**
     * @param int $statusCode
     */
    public function setStatus(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }
}
