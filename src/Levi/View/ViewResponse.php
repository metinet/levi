<?php declare(strict_types=1);

namespace Levi\View;

use Levi\Http\StatusCode;

/**
 * Class ViewResponse
 * @package Levi\View
 */
class ViewResponse
{
    /** @var string */
    private $content;
    /** @var int */
    private $statusCode;

    /**
     * ViewResponse constructor.
     * @param string $content
     * @param int|null $statusCode
     */
    private function __construct(string $content, int $statusCode = null)
    {
        $this->content = $content;
        $this->statusCode = $statusCode ?? StatusCode::HTTP_OK;
    }

    /**
     * @param string $content
     * @param int|null $statusCode
     * @return ViewResponse
     */
    public static function create(string $content, int $statusCode = null): self
    {
        return new self($content, $statusCode);
    }

    /**
     * @param int $status
     * @return ViewResponse
     */
    public function withStatus(int $status): self
    {
        $this->statusCode = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return bool
     */
    public function isNotFound(): bool
    {
        return $this->statusCode === StatusCode::HTTP_NOT_FOUND;
    }
}
