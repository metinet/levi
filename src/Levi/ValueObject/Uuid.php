<?php declare(strict_types=1);

namespace Levi\ValueObject;

use Levi\Exception\InvalidUuidException;
use Ramsey\Uuid\Uuid as RamseyUuid;

/**
 * Class Uuid
 * @package Levi
 */
class Uuid
{
    /** @var string */
    private $uuid;

    /**
     * Uuid constructor.
     * @param $uuid
     * @throws InvalidUuidException
     */
    private function __construct($uuid)
    {
        if (!RamseyUuid::isValid($uuid)) {
            throw new InvalidUuidException('UUID not valid');
        }
        $this->uuid = $uuid;
    }

    /**
     * @param $uuid
     * @return Uuid
     * @throws InvalidUuidException
     */
    public static function fromString($uuid): Uuid
    {
        return new self($uuid);
    }

    /**
     * @return Uuid
     * @throws InvalidUuidException
     */
    public static function generate(): Uuid
    {
        return new self(RamseyUuid::uuid4()->toString());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->uuid;
    }
}
