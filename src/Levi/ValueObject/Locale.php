<?php declare(strict_types=1);

namespace Levi\ValueObject;

use InvalidArgumentException;

/**
 * Class Locale
 * @package Levi\ValueObject
 */
class Locale
{
    /** @var string */
    private $locale;
    /** @var string */
    private $language;
    /** @var string */
    private $country;

    /**
     * Locale constructor.
     * @param string $locale
     * @throws \InvalidArgumentException
     */
    private function __construct(string $locale)
    {
        // string and ISO-639 ISO-3166 validation de_DE
        if (\preg_match('/^([a-z]{2})[_-]([A-Z]{2,})$/', $locale, $match) === 0) {
            throw new InvalidArgumentException('Locale has wrong format:' . $locale);
        }
        $this->locale = $locale;
        $this->language = $match[1] ?? null;
        $this->country = $match[2] ?? null;
    }

    /**
     * @param $locale
     * @return Locale
     * @throws \InvalidArgumentException
     */
    public static function fromString($locale): self
    {
        return new self($locale ?? '');
    }

    /**
     * @return string
     * @deprecated uses toString
     * @codeCoverageIgnore
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
