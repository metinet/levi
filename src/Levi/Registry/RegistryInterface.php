<?php declare(strict_types=1);

namespace Levi\Registry;

use Levi\Config\ConfigProviderInterface;

/**
 * Interface RegistryInterface
 * @package Levi\Registry
 */
interface RegistryInterface
{
    /**
     * DiInterface constructor.
     * @param ConfigProviderInterface $config
     */
    public function __construct(ConfigProviderInterface $config);
}
