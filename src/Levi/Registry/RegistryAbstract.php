<?php declare(strict_types=1);

namespace Levi\Registry;

use Levi\Config\ConfigProviderInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Class RegistryAbstract
 * @package Levi\Registry
 */
abstract class RegistryAbstract implements RegistryInterface
{
    /** @var ConfigProviderInterface */
    private $config;
    /** @var Logger[] */
    private $logger;

    /**
     * RegistryAbstract constructor.
     * @param ConfigProviderInterface $config
     */
    public function __construct(ConfigProviderInterface $config)
    {
        $this->config = $config;
    }

    /**
     * @param string|null $name
     * @return Logger
     * @throws \Exception
     */
    public function getLogger(string $name = null): Logger
    {
        $name = $name ?? 'ops';
        if (empty($this->logger[$name])) {
            $this->logger[$name] = $this->createLogger($name);
        }

        return $this->logger[$name];
    }

    /**
     * @param string $name
     * @return Logger
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    protected function createLogger(string $name): Logger
    {
        $config = $this->getConfig()->getApplicationConfig();
        $logger = new Logger($name);
        $logfile = $config->getLogPath() . '/' . $name . '.log';
        $logger->pushHandler(new StreamHandler($logfile));

        return $logger;
    }

    /**
     * @return ConfigProviderInterface
     */
    protected function getConfig(): ConfigProviderInterface
    {
        return $this->config;
    }
}
