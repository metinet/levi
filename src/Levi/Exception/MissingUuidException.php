<?php declare(strict_types=1);

namespace Levi\Exception;

use InvalidArgumentException;

/**
 * Class MissingUuidException
 * @package Levi\Exception
 */
class MissingUuidException extends InvalidArgumentException implements ExceptionInterface
{

}
