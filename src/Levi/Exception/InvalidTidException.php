<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class InvalidTidException
 * @package Levi\Exception
 */
class InvalidTidException extends Exception implements ExceptionInterface
{

}
