<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class InvalidRidException
 * @package Levi\Exception
 */
class InvalidRidException extends Exception implements ExceptionInterface
{

}
