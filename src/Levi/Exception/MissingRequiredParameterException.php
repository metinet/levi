<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class MissingRequiredParameterException
 * @package Levi\Exception
 */
class MissingRequiredParameterException extends Exception implements ExceptionInterface
{

}
