<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class MissingViewTemplateException
 * @package Levi\Exception
 */
class MissingViewTemplateException extends Exception implements ExceptionInterface
{

}
