<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class MarketingCloudExeption
 * @package Levi\Exception
 */
class MarketingCloudExeption extends Exception implements ExceptionInterface
{
}
