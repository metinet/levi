<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class InvalidGidException
 * @package Levi\Exception
 */
class InvalidGidException extends Exception implements ExceptionInterface
{

}
