<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class AuthenticationException
 * @package Levi\Exception
 */
class AuthenticationException extends Exception implements ExceptionInterface
{

}
