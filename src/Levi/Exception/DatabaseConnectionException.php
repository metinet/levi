<?php declare(strict_types=1);

namespace Levi\Exception;

use InvalidArgumentException;

/**
 * Class DatabaseConnectionException
 * @package Levi\Exception
 */
class DatabaseConnectionException extends InvalidArgumentException implements ExceptionInterface
{

}
