<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class ItemAlreadyExistsException
 * @package Levi\Exception
 */
class ItemAlreadyExistsException extends Exception implements ExceptionInterface
{

}
