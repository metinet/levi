<?php declare(strict_types=1);

namespace Levi\Exception;

use InvalidArgumentException;

/**
 * Class InvalidUuidException
 * @package Levi\Exception
 */
class InvalidUuidException extends InvalidArgumentException implements ExceptionInterface
{

}
