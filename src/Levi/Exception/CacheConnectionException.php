<?php declare(strict_types=1);

namespace Levi\Exception;

use InvalidArgumentException;

/**
 * Class CacheConnectionException
 * @package Levi\Exception
 */
class CacheConnectionException extends InvalidArgumentException implements ExceptionInterface
{

}
