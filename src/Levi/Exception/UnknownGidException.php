<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class UnknownGidException
 * @package Levi\Exception
 */
class UnknownGidException extends Exception implements ExceptionInterface
{

}
