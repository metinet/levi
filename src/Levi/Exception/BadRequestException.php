<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class BadRequestException
 * @package Levi\Exception
 */
class BadRequestException extends Exception implements ExceptionInterface
{

}
