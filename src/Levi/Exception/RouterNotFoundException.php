<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class RouterNotFoundException
 * @package Levi\Exception
 */
class RouterNotFoundException extends Exception implements ExceptionInterface
{

}
