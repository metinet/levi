<?php declare(strict_types=1);

namespace Levi\Exception;

use Exception;

/**
 * Class ServiceUnavailableException
 * @package Levi\Exception
 */
class ServiceUnavailableException extends Exception implements ExceptionInterface
{
}
