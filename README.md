# README #

Levi Framework - PHP Web Components 

### What is this repository for? ###

* Holds a couple of helper classes and interfaces to make work easier in PHP based web projects and APIs.

### How do I get set up? ###

* composer require metinet/levi

### Contribution guidelines ###

* Assl classes should have a test coverage of 100%. But you may insert ignore instructions ;)

### Who do I talk to? ###

* info@metinet.de