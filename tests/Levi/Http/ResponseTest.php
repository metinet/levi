<?php declare(strict_types=1);

namespace Levi\Http;

use PHPUnit\Framework\TestCase;

/**
 * Class ResponseTest
 * @preserveGlobalState enabled
 * @package             Levi\Http
 * @covers     \Levi\Http\Response
 * @uses       \Levi\Http\Response
 */
class ResponseTest extends TestCase
{
    public function testCanAddVersion()
    {
        $response = new Response();
        $response->addVersion('version-name');
        self::assertTrue(true);
    }

    /**
     */
    public function testBadRequestReturnsResponse()
    {
        $response = Response::badRequest('-test-content-');
        self::assertInstanceOf(Response::class, $response);
        self::assertSame('-test-content-', $response->getContent());
        self::assertSame(400, $response->getStatusCode());
        self::assertSame(
            '-test-content-',
            $response->getContent()
        );
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testNotFoundReturnsResponse()
    {
        $jsonResponse = Response::notFound();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(404, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testUnprocessableEntityReturnsResponse()
    {
        $jsonResponse = Response::unprocessableEntity();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(422, $jsonResponse->getStatusCode());
    }

//    /**
//     * @preserveGlobalState disabled
//     */
//    public function testHateoasReturnsResponse()
//    {
//        self::assertInstanceOf(HateoasResponse::class, Response::hateoas([]));
//    }

    /**
     * @preserveGlobalState enabled
     */
    public function testNotModifiedReturnsResponse()
    {
        $jsonResponse = Response::notModified();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(304, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testServiceUnavailableReturnsResponse()
    {
        $jsonResponse = Response::serviceUnavailable('test');
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(503, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testInternalServerErrorReturnsResponse()
    {
        $jsonResponse = Response::internalServerError('test');
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(500, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnResponseOnUnauthorized()
    {
        $jsonResponse = Response::unauthorized();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(401, $jsonResponse->getStatusCode());
        self::assertSame('test-content', Response::unauthorized('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnResponseOnMethodNotAllowed()
    {
        $jsonResponse = Response::methodNotAllowed();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(Response::HTTP_METHOD_NOT_ALLOWED, $jsonResponse->getStatusCode());
        self::assertSame(('test-content'), Response::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnResponseOk()
    {
        $jsonResponse = Response::ok();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(Response::HTTP_OK, $jsonResponse->getStatusCode());
        self::assertSame(('test-content'), Response::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnResponseNoContent()
    {
        $jsonResponse = Response::noContent();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(Response::HTTP_NO_CONTENT, $jsonResponse->getStatusCode());
        self::assertSame('test-content', Response::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnResponseConflict()
    {
        $jsonResponse = Response::conflict();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(Response::HTTP_CONFLICT, $jsonResponse->getStatusCode());
        self::assertSame('test-content', Response::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnResponseCreated()
    {
        $jsonResponse = Response::created();
        self::assertInstanceOf(Response::class, $jsonResponse);
        self::assertSame(Response::HTTP_CREATED, $jsonResponse->getStatusCode());
        self::assertSame('test-content', Response::methodNotAllowed('test-content')->getContent());
    }

}
