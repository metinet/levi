<?php declare(strict_types=1);

namespace Levi\Http;

use PHPUnit\Framework\TestCase;

/**
 * Class JsonResponseTest
 * @package             Levi\Http
 * @covers     \Levi\Http\JsonResponse
 * @uses       \Levi\Http\HateoasJsonResponse
 * @preserveGlobalState enabled
 */
class JsonResponseTest extends TestCase
{
    /**
     */
    public function testBadRequestReturnsJsonResponse()
    {
        $jsonResponse = JsonResponse::badRequest('-test-content-');
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(json_encode('-test-content-'), $jsonResponse->getContent());
        self::assertSame(400, $jsonResponse->getStatusCode());
        self::assertSame(
            '"-test-content-"',
            $jsonResponse->getContent()
        );
    }

    /**
     * @preserveGlobalState disabled
     */
    public function testNotFoundReturnsJsonResponse()
    {
        $jsonResponse = JsonResponse::notFound();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(404, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testNotModifiedReturnsJsonResponse()
    {
        $jsonResponse = JsonResponse::notModified();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(304, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testServiceUnavailableReturnsJsonResponse()
    {
        $jsonResponse = JsonResponse::serviceUnavailable('test');
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(503, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testInternalServerErrorReturnsJsonResponse()
    {
        $jsonResponse = JsonResponse::internalServerError('test');
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(500, $jsonResponse->getStatusCode());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnJsonResponseOnUnauthorized()
    {
        $jsonResponse = JsonResponse::unauthorized();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(401, $jsonResponse->getStatusCode());
        self::assertSame(json_encode('test-content'), JsonResponse::unauthorized('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnJsonResponseOnMethodNotAllowed()
    {
        $jsonResponse = JsonResponse::methodNotAllowed();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(JsonResponse::HTTP_METHOD_NOT_ALLOWED, $jsonResponse->getStatusCode());
        self::assertSame(json_encode('test-content'), JsonResponse::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnJsonResponseOk()
    {
        $jsonResponse = JsonResponse::ok();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(JsonResponse::HTTP_OK, $jsonResponse->getStatusCode());
        self::assertSame(json_encode('test-content'), JsonResponse::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnJsonResponseNoContent()
    {
        $jsonResponse = JsonResponse::noContent();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(JsonResponse::HTTP_NO_CONTENT, $jsonResponse->getStatusCode());
        self::assertSame(json_encode('test-content'), JsonResponse::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnJsonResponseConflict()
    {
        $jsonResponse = JsonResponse::conflict();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(JsonResponse::HTTP_CONFLICT, $jsonResponse->getStatusCode());
        self::assertSame(json_encode('test-content'), JsonResponse::methodNotAllowed('test-content')->getContent());
    }

    /**
     * @preserveGlobalState enabled
     */
    public function testCanReturnJsonResponseCreated()
    {
        $jsonResponse = JsonResponse::created();
        self::assertInstanceOf(JsonResponse::class, $jsonResponse);
        self::assertSame(JsonResponse::HTTP_CREATED, $jsonResponse->getStatusCode());
        self::assertSame(json_encode('test-content'), JsonResponse::methodNotAllowed('test-content')->getContent());
    }
}
