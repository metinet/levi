<?php declare(strict_types=1);

namespace Levi\Http;

use PHPUnit\Framework\TestCase;

/**
 * Class RequestTest
 * @package Levi\Http
 * @covers     \Levi\Http\Request
 */
class RequestTest extends TestCase
{
    public function testCanCreateFromGlobals()
    {
        $request = Request::createFromGlobals();
        self::assertInstanceOf(Request::class, $request);
    }
}
